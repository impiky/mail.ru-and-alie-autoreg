import datetime
import requests
from selenium import webdriver
from ctypes import *
import os
import sys
import pyodbc
import time
import configparser
import uuid as uuid
import random
import http.client
import mimetypes
import urllib
import json
import time
import requests
import subprocess
import time


# иЋ·еѕ—и„љжњ¬й…ЌзЅ®ж–‡д»¶
def getConfig(section, key):
    config = configparser.ConfigParser()
    inipath = os.path.dirname(os.path.realpath(sys.executable))
    config.read("config.ini", encoding="utf-8")
    # config.read(inipath + "\\config.ini", encoding="utf-8")
    return config.get(section, key)


# иЋ·еѕ—и™љж‹џжњєй…ЌзЅ®ж–‡д»¶
def getVMConfig(section, key):
    config = configparser.ConfigParser()
    config.read("C:\\abc\\ABCMOBI.ini", encoding="utf-8")
    return config.get(section, key)


Driver = getConfig("database", "Driver")
SERVER = getConfig("database", "SERVER")
DATABASE = getConfig("database", "DATABASE")
UID = getConfig("database", "UID")
PWD = getConfig("database", "PWD")
yzmuser = getConfig("vcode", "username")
yzmpassword = getConfig("vcode", "password")
chromepath = getConfig("chrome", "chromedriverpath")


# йЄЊиЇЃз Ѓж“ЌдЅњз±»
class YDMHttp:
    API_URL = 'http://api.yundama.com/api.php'
    username = ''
    password = ''
    appid = ''
    appkey = ''

    def __init__(self, username, password, appid, appkey):
        self.username = username
        self.password = password
        self.appid = str(appid)
        self.appkey = appkey

    def request(self, fields, files=[]):
        response = self.post_url(self.API_URL, fields, files)
        response = json.loads(response)
        return response

    def balance(self):
        data = {'method': 'balance', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey}
        response = self.request(data)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['balance']
        else:
            return -9001

    def login(self):
        data = {'method': 'login', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey}
        response = self.request(data)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['uid']
        else:
            return -9001

    def upload(self, filename, codetype, timeout):
        data = {'method': 'upload', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'codetype': str(codetype), 'timeout': str(timeout)}
        file = {'file': filename}
        response = self.request(data, file)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['cid']
        else:
            return -9001

    def result(self, cid):
        data = {'method': 'result', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'cid': str(cid)}
        response = self.request(data)
        return response and response['text'] or ''

    def decode(self, filename, codetype, timeout):
        cid = self.upload(filename, codetype, timeout)
        if (cid > 0):
            for i in range(0, timeout):
                result = self.result(cid)
                if (result != ''):
                    return cid, result
                else:
                    time.sleep(1)
            return -3003, ''
        else:
            return cid, ''

    def report(self, cid):
        data = {'method': 'report', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'cid': str(cid), 'flag': '0'}
        response = self.request(data)
        if (response):
            return response['ret']
        else:
            return -9001

    def post_url(self, url, fields, files=[]):
        for key in files:
            files[key] = open(files[key], 'rb')
        res = requests.post(url, files=files, data=fields)
        return res.text


# ж•°жЌ®еє“ж“ЌдЅњз±»
class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # иїћжЋҐж•°жЌ®еє“
    def connectDatabase(self):
        connection_data = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                                 self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(connection_data)
            self.cur = self.conn.cursor()
            return True
        except:
            print("connect fail")
            return False

    # е…ій—­ж•°жЌ®еє“
    def close(self):
        # е¦‚жћњж•°жЌ®ж‰“ејЂпјЊе€™е…ій—­пј›еђ¦е€™жІЎжњ‰ж“ЌдЅњ
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    # ж‰§иЎЊж•°жЌ®еє“зљ„sqиЇ­еЏҐ,дё»и¦Ѓз”ЁжќҐеЃљжЏ’е…Ґж“ЌдЅњ
    def execute(self, sql, params=None, commit=False, ):
        # иїћжЋҐж•°жЌ®еє“
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # ж­ЈеёёйЂ»иѕ‘пјЊж‰§иЎЊsqlпјЊжЏђдє¤ж“ЌдЅњ
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                # if commit:
                #     self.conn.commit()
                # else:
                #     pass
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    # жџҐиЇўж‰Ђжњ‰ж•°жЌ®
    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("жџҐиЇўе¤±иґҐ")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results

    # жџҐиЇўдёЂжќЎж•°жЌ®
    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("Query not executed")
            return False
        result = self.cur.fetchone()
        # print("жџҐиЇўж€ђеЉџ" + str(result))
        self.close()
        return result

    # еўће€ ж”№ж•°жЌ®
    def edit(self, sql, params=None):
        res = self.execute(sql, params, True)
        if not res:
            print("ж“ЌдЅње¤±иґҐ")
            return False
        self.conn.commit()
        print("ж“ЌдЅњж€ђеЉџ" + str(res))
        self.close()
        return res


# з”џж€ђиґ¦еЏ·
def getaccount(a, b, c, d, e):
    li = [a, b, c, d, e]
    random.shuffle(li)
    # и°ѓз”ЁrandomжЁЎеќ—зљ„shuffleе‡Ѕж•°ж‰“д№±е€—иЎЁ
    return ''.join(li)


# з”џж€ђеЇ†з Ѓ
def getpasssord():
    uid = str(uuid.uuid1())
    suid = ''.join(uid.split('-'))
    password4 = random.sample(suid, random.randint(6, 20))
    return ''.join(password4)


# иЋ·еѕ—йЄЊиЇЃз Ѓ
def getyzm(filename):
    # з”Ёж€·еђЌ
    username = yzmuser
    # еЇ†з Ѓ
    password = yzmpassword
    # иЅЇд»¶пј©пј¤пјЊејЂеЏ‘иЂ…е€†ж€ђеї…и¦ЃеЏ‚ж•°гЂ‚з™»еЅ•ејЂеЏ‘иЂ…еђЋеЏ°гЂђж€‘зљ„иЅЇд»¶гЂ‘иЋ·еѕ—пјЃ
    appid = 1
    # иЅЇд»¶еЇ†й’ҐпјЊејЂеЏ‘иЂ…е€†ж€ђеї…и¦ЃеЏ‚ж•°гЂ‚з™»еЅ•ејЂеЏ‘иЂ…еђЋеЏ°гЂђж€‘зљ„иЅЇд»¶гЂ‘иЋ·еѕ—пјЃ
    APP_KEY = '22cc5376925e9387a23cf797cb9ba745'
    # е›ѕз‰‡ж–‡д»¶
    # filename = 'C:\\Users\\lsy\\Desktop\\Pythonи°ѓз”Ёз¤єдѕ‹\\ж–°е»єж–‡д»¶е¤№\\PythonHTTPи°ѓз”Ёз¤єдѕ‹\\getimage.jpg'
    # йЄЊиЇЃз Ѓз±»ећ‹пјЊ# дѕ‹пјљ1004иЎЁз¤є4дЅЌе­—жЇЌж•°е­—пјЊдёЌеђЊз±»ећ‹ж”¶иґ№дёЌеђЊгЂ‚иЇ·е‡†зЎ®еЎ«е†™пјЊеђ¦е€™еЅ±е“ЌиЇ†е€«зЋ‡гЂ‚ењЁж­¤жџҐиЇўж‰Ђжњ‰з±»ећ‹ http://www.yundama.com/price.html
    codetype = 1006
    # и¶…ж—¶ж—¶й—ґпјЊз§’
    timeout = 60
    # жЈЂжџҐ
    if username == 'username':
        print('иЇ·и®ѕзЅ®еҐЅз›ёе…іеЏ‚ж•°е†Ќжµ‹иЇ•')
    else:
        # е€ќе§‹еЊ–
        yundama = YDMHttp(username, password, appid, APP_KEY)
        # з™»й™†дє‘ж‰“з Ѓ
        uid = yundama.login()
        print('uid: %s' % uid)
        # жџҐиЇўдЅ™йўќ
        balance = yundama.balance()
        print('balance: %s' % balance)
        # ејЂе§‹иЇ†е€«пјЊе›ѕз‰‡и·Їеѕ„пјЊйЄЊиЇЃз Ѓз±»ећ‹IDпјЊи¶…ж—¶ж—¶й—ґпј€з§’пј‰пјЊиЇ†е€«з»“жћњ
        cid, result = yundama.decode(filename, codetype, timeout)
        print('cid: %s, result: %s' % (cid, result))
        return result


# дї®ж”№е€†иѕЁзЋ‡
def setscreen(x, y):
    try:
        cdll = os.path.dirname(os.path.realpath(sys.executable)) + "\\Dll1.dll"
        # cdll='C:\\Users\\lsy\\Desktop\\Pythonи°ѓз”Ёз¤єдѕ‹\\Dll1.dll'
        h = windll.LoadLibrary(cdll)
        a = h.setscreen(x, y)
        return
    except:
        return


# ######################################е®ћй™…ж“ЌдЅњ#################################################
scriptname = "жіЁе†Њй‚®з®±"
scriptversion = "1.0"


# е†™ж—Ґеї—
def setlog(LoID, logvalue, dbmanage, level):
    dbmanage.execute(
        "INSERT INTO Log (LogID,ScriptName,times,info,ScriptVersion,LogType) VALUES ('{}','{}','{}','{}','{}','{}')".format(LoID, scriptname,
                                                                                                                            str(datetime.datetime.now()), logvalue, scriptversion, level))


# е†™и„љжњ¬иїђиЎЊзЉ¶жЂЃ
def setScriptStatus(status, dbmanage, vm, level, Runtimes):
    dbmanage.execute(
        "Update Control set ScriptStatus='{}',Runtimes='{}',ScriptStatusType='{}' WHERE VMID='{}'".format(status, str(Runtimes), level, vm))


def downloadimg(imagurl, imagepath, useragent, cookie):
    try:
        print("дё‹иЅЅе›ѕз‰‡")
        header = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
        }
        cookiess = [item["name"] + "=" + item["value"] for item in cookie]
        cookiestr = '; '.join(item for item in cookiess)
        header.update({'Cookie': cookiestr})
        header.update({'user-agent': useragent})
        # imgurl = "https://c.mail.ru/c/6?0.556070617381563"
        # imagpath = "D:\\test\\yzm.png"
        r = requests.get(imagurl, headers=header)
        with open(imagepath, 'wb') as f:
            f.write(r.content)
    except Exception as e:
        print(e)


def setintnet(se):
    # se 0дёєе…ій—­зЅ‘з»њ   1дёєж‰“ејЂзЅ‘з»њ
    # 0 set internet connection, 1 reset internet connection
    cmd_connect_command_velcom = ['rasdial',
                                  'Velcom PPPoE', '000203117005', '2063222']
    cmd_disconnect_command_velcom = ['rasdial', 'Velcom PPPoE', '/disconnect']
    if se == 0:
        print("зЅ‘з»њи®ѕзЅ®е¤±иґҐ")
    elif se == 1:
        subprocess.Popen(cmd_connect_command_velcom, stdout=subprocess.PIPE)
        return True


# resets velcom pppoe
def resetintnet():
    cmd_connect_command_velcom = ['rasdial',
                                  'Velcom PPPoE', '000203117005', '2063222']
    cmd_disconnect_command_velcom = ['rasdial', 'Velcom PPPoE', '/disconnect']
    subprocess.Popen(cmd_disconnect_command_velcom, stdout=subprocess.PIPE)
    time.sleep(2)
    subprocess.Popen(cmd_connect_command_velcom, stdout=subprocess.PIPE)


# й‡Ќе†™ж‰§иЎЊjsж–№жі•
def execute_script(browser, js):
    try:
        return browser.execute_script(js)
    except:
        return ''


# йљЏжњєзЄ—еЏЈе¤§е°Џ
def setbrowszie(browser):
    fbl = [[1024, 600], [1024, 600], [1024, 600], [1024, 768], [1280, 1024], [1600, 900], [1440, 1050], [1600, 1200],
           [1280, 800], [1366, 768], [1280, 854], [1440, 900], [1600, 1024], [1680, 1050], [1920, 1080]]
    random.shuffle(fbl)
    browser.set_window_size(fbl[0][0], fbl[0][1])


# и®ѕзЅ®жµЏи§€е™Ёе’Њи®ѕзЅ®зЋЇеўѓ
def setbrowser(useragent, byid):
    # й…ЌзЅ®
    options = webdriver.ChromeOptions()
    # и®ѕзЅ®жµЏи§€е™ЁиЇ­иЁЂ
    options.add_argument('lang=ru-ru.UTF-8')
    options.add_experimental_option('excludeSwitches', ['enable-automation'])
    # и®ѕзЅ®жµЏи§€е™Ёз±»ећ‹
    options.add_argument('user-agent={}'.format(useragent))
    # и®ѕзЅ®cookieе’Њзј“е­дїќе­и·Їеѕ„
    options.add_argument(r"user-data-dir=" + "C:\\chromedata\\" + byid)
    try:
        browser = webdriver.Chrome(
            chrome_options=options, executable_path=chromepath)
    except Exception as e1:
        print(chromepath + "ж–‡д»¶дёЌе­ењЁ")
        print(e1)
        return False
    setbrowszie(browser)
    return browser


# жіЁе†Њmail.ruжЁЎеќ—
def registermail(vmid, browser, byid, surname, name, mailetype, useragent, yzmpath, dbManagers, starttime):
    year = ""
    month = ""
    day = ""
    account = ""
    password = ""
    var = 1
    logid = str(int(time.time()))
    browser.get("https://account.mail.ru/signup?from=main&rf=auth.mail.ru")
    setlog(logid, "ж‰“ејЂдє†mailзљ„дё»йЎµ", dbManagers, "A")
    setScriptStatus("ж‰“ејЂдє†mailзљ„дё»йЎµ", dbManagers, vmid, "A", int(
        (datetime.datetime.now() - starttime).total_seconds()))
    time.sleep(5)
    # з‚№е‡»жіЁе†ЊжЊ‰й’®
    # browser.find_element_by_id("PH_regLink").click()
    while var < 100:
        try:
            var = var + 1
            time.sleep(1)
            if var > 50:
                print("еј‚еёёеѕЄзЋЇ")
                setlog(logid, "еЎ«иЎЁеј‚еёёеѕЄзЋЇпјЊй‡ЌеђЇи„љжњ¬", dbManagers, "C")
                return False
            setlog(logid, "иї›е…Ґз”іиЇ·йЎµ", dbManagers, "A")
            setlog(logid, "ејЂе§‹жіЁе†Њй‚®з®±", dbManagers, "A")
            if len(execute_script(browser, "return document.querySelector('.b-panel__content__desc').innerText;")) > 0:
                print("иї›е…ҐйЄЊиЇЃз Ѓз•Њйќў")
                setScriptStatus("иї›е…ҐйЄЊиЇЃз Ѓз•Њйќў", dbManagers, vmid, "A",
                                int((datetime.datetime.now() - starttime).total_seconds()))
                break
            if len(execute_script(browser, "return document.querySelector('.js-signup-simple-link').innerText;")) <= 0:
                if len(execute_script(browser, "return document.querySelector('.b-phone__number').className;")) > 0:
                    print("йњЂи¦Ѓж‰‹жњєеЏ·з Ѓ")
                    return False
            # browser.get("https://account.mail.ru/signup?from=navi&lang=ruRU&siteid=169&rnd=585367170")
            # иЋ·еЏ–cookie
            cookies = browser.get_cookies()
            time.sleep(5)
            # иѕ“е…Ґе§“еђЌ
            browser.execute_script(
                "document.querySelector('[name=firstname]').value='{}';".format(surname))
            browser.execute_script(
                "document.querySelector('[name=lastname]').value='{}';".format(name))
            setlog(logid, "иѕ“е…Ґе§“еђЌ", dbManagers, "A")
            setScriptStatus("иѕ“е…Ґе§“еђЌ", dbManagers, vmid, "A", int(
                (datetime.datetime.now() - starttime).total_seconds()))
            # е‡єз”џж—Ґжњџ
            # жњ€
            monthrand = str(random.randint(0, 11))
            month = str(random.randint(0, 11) + 1)
            browser.execute_script(
                "document.querySelector('.b-date__month').querySelectorAll('.b-dropdown__list__item')[{}].click();".format(monthrand))
            browser.execute_script(
                "document.querySelector('.b-date__year').querySelectorAll('.b-dropdown__list__item')[{}].click();".format(str(random.randint(19, 55))))
            # е№ґ
            year = browser.execute_script(
                "return document.querySelector('.b-date__year').innerText;")
            browser.execute_script(
                "document.querySelector('.day{}').click();".format(str(random.randint(1, 31))))
            # е¤©
            day = browser.execute_script(
                "return document.querySelector('.b-date__day').innerText;")
            setlog(logid, "иѕ“е…Ґз”џж—Ґ", dbManagers, "A")
            setScriptStatus("иѕ“е…Ґз”џж—Ґ", dbManagers, vmid, "A", int(
                (datetime.datetime.now() - starttime).total_seconds()))
            # йЂ‰ж‹©жЂ§е€«
            browser.execute_script("document.querySelectorAll('[name=sex]')[{}].click();".format(
                str(random.randint(0, 1))))
            setlog(logid, "йЂ‰ж‹©жЂ§е€«", dbManagers, "A")
            setScriptStatus("йЂ‰ж‹©жЂ§е€«", dbManagers, vmid, "A", int(
                (datetime.datetime.now() - starttime).total_seconds()))
            # йЂ‰ж‹©й‚®з®±з±»ећ‹
            browser.execute_script(
                "document.querySelector('.b-email__domain').querySelectorAll('.b-dropdown__list__item')[{}].click();".format(str(mailetype)))
            time.sleep(4)
            # иѕ“е…Ґиґ¦еЏ·
            browser.execute_script(
                "document.querySelector('.b-email__name').querySelector('.b-input_disallow-custom-domain').value='';")
            acnum = browser.execute_script(
                "return document.querySelectorAll('.b-list__item__content').length;")
            randnum = random.randint(0, int(acnum) - 1)
            account = browser.execute_script(
                "return document.querySelectorAll('.b-list__item__content')[{}].innerText;".format(str(randnum)))
            print(account)
            browser.execute_script(
                "document.querySelectorAll('.b-list__item__content')[{}].click();".format(str(randnum)))
            setlog(logid, "иѕ“е…Ґиґ¦еЏ·", dbManagers, "A")
            setScriptStatus("иѕ“е…Ґиґ¦еЏ·:" + str(account), dbManagers, vmid, "A",
                            int((datetime.datetime.now() - starttime).total_seconds()))
            time.sleep(5)
            # иѕ“е…ҐеЇ†з Ѓ
            password = getpasssord()
            browser.execute_script(
                "document.querySelector('.b-input_plate').value='';")
            browser.find_element_by_class_name(
                "b-input_plate").send_keys(str(password))
            setlog(logid, "иѕ“е…ҐеЇ†з Ѓ", dbManagers, "A")
            setScriptStatus("иѕ“е…ҐеЇ†з Ѓ:" + str(password), dbManagers, vmid, "A",
                            int((datetime.datetime.now() - starttime).total_seconds()))
            time.sleep(5)
            # е†Ќж¬Ўиѕ“е…ҐеЇ†з Ѓ
            browser.execute_script(
                "document.querySelector('[name=password_retry]').value='';")
            browser.find_element_by_name(
                "password_retry").send_keys(str(password))
            time.sleep(5)
            # з‚№е‡»жІЎжњ‰ж‰‹жњє
            execute_script(
                browser, "document.querySelector('.js-signup-simple-link').click();")
            # з‚№е‡»жЏђдє¤
            browser.find_element_by_class_name("btn_responsive-wide").click()
            time.sleep(3)
            setlog(logid, "иѕ“е…Ґе†…е®№е®ЊжЇ•", dbManagers, "A")
            setScriptStatus("иѕ“е…Ґе†…е®№е®ЊжЇ•", dbManagers, vmid, "A", int(
                (datetime.datetime.now() - starttime).total_seconds()))
        except:
            continue
    var2 = 1
    yzmnum = 0
    while var2 < 100:
        try:
            setlog(logid, "ејЂе§‹йЄЊиЇЃз Ѓ", dbManagers, "A")
            setScriptStatus("ејЂе§‹йЄЊиЇЃз Ѓ", dbManagers, vmid, "A", int(
                (datetime.datetime.now() - starttime).total_seconds()))
            var2 = var2 + 1
            time.sleep(1)
            if var2 > 50:
                print("еј‚еёёеѕЄзЋЇ")
                setlog(logid, "и„љжњ¬е› йЄЊиЇЃз ЃеѕЄзЋЇи§¦еЏ‘й‡ЌеђЇи„љжњ¬", dbManagers, "C")
                setScriptStatus("и„љжњ¬е› йЄЊиЇЃз ЃеѕЄзЋЇи§¦еЏ‘й‡ЌеђЇи„љжњ¬", dbManagers, vmid, "C",
                                int((datetime.datetime.now() - starttime).total_seconds()))
                return False
            if len(execute_script(browser,
                                  "return document.querySelector('.search-panel-button').innerText;")) > 0 or len(
                    execute_script(browser, "return document.querySelector('.js-next-step-button').innerText;")) > 0:
                print("жіЁе†Њж€ђеЉџ")
                setlog(logid, "йЄЊиЇЃж€ђеЉџпјЊжњ¬ж¬ЎйЄЊиЇЃдє†" + str(yzmnum) + "ж¬Ў", dbManagers, "A")
                setScriptStatus("йЄЊиЇЃж€ђеЉџпјЊжњ¬ж¬ЎйЄЊиЇЃдє†" + str(yzmnum) + "ж¬Ў", dbManagers, vmid, "A",
                                int((datetime.datetime.now() - starttime).total_seconds()))
                break

            if len(execute_script(browser, "return document.querySelector('.b-form-field__label').innerText;")) > 0:
                print("йњЂи¦Ѓиѕ“е…Ґж‰‹жњє")  # need to provide mobile phone
                setlog(logid, "йњЂи¦Ѓиѕ“е…Ґж‰‹жњєй‡ЌеђЇи„љжњ¬", dbManagers, "C")
                resetintnet()
                time.sleep(5)
                return False
                # жµ‹иЇ•з”Ёз›ґжЋҐйЂЂе‡є
                # break
            src = execute_script(
                browser, "return document.querySelector('.b-captcha__captcha').src;")
            time.sleep(5)
            # дё‹иЅЅйЄЊиЇЃз Ѓе›ѕз‰‡
            if len(src) > 0:
                imagepath = yzmpath + "\\" + "yzm.png"
                print(imagepath)
                if os.path.exists(imagepath):
                    print("deleteyzm")
                    os.remove(imagepath)
                print(src)
                downloadimg(src, imagepath, useragent, cookies)
                time.sleep(8)
                if os.path.exists(imagepath):
                    print("startyzm")
                    # йЄЊиЇЃз Ѓ
                    yzmresult = getyzm(imagepath)
                    yzmnum = yzmnum + 1
                    if yzmnum > 10:
                        setlog(logid, "и„љжњ¬е› йЄЊиЇЃз ЃеѕЄзЋЇи§¦еЏ‘й‡ЌеђЇи„љжњ¬", dbManagers, "C")
                        setScriptStatus("и„љжњ¬е› йЄЊиЇЃз ЃеѕЄзЋЇи§¦еЏ‘й‡ЌеђЇи„љжњ¬", dbManagers, vmid, "C",
                                        int((datetime.datetime.now() - starttime).total_seconds()))
                        return False
                    time.sleep(5)
                    print(yzmresult)
                    # иѕ“е…ҐйЄЊиЇЃз Ѓ
                    execute_script(browser,
                                   "document.querySelector('.b-input_captcha').value='" + str(yzmresult) + "';")
                    setScriptStatus("иѕ“е…ҐйЄЊиЇЃз Ѓ:" + str(yzmresult), dbManagers, vmid, "A",
                                    int((datetime.datetime.now() - starttime).total_seconds()))
                    # жЏђдє¤
                    execute_script(
                        browser, "document.querySelector('.btn_main').click();")
                    time.sleep(10)
                    if len(execute_script(browser,
                                          "return document.querySelector('.b-captcha__error-msg').innerText;")) > 0:
                        print("йЄЊиЇЃз Ѓй”™иЇЇ")
                        browser.exec_js(
                            "document.querySelector('.b-captcha__code__reload').click();")
                        time.sleep(5)
        except:
            continue
    # dbManagers.execute(
    #     "UPDATE Buyer SET birthdayyear='" + year + "',birthdaymonth='" + month + "',EmailRegisteredTime='" + str(
    #         datetime.datetime.now()) + "',birthdayday='" + day + "', Email='" + account + "', PassWord='" + password + "' WHERE BuyerID='" + byid + "'")
    # dbManagers.close()
    if account != "":
        return year, month, day, account, password
    else:
        return False


# login to mail.ru
def login_to_mail(dbManager, byid, browser):
    browser.get("https://mail.ru/")
    mail_ru_data = dbManager.fetchone(
        "SELECT Email, Password FROM Buyer where BuyerID ='{}'".format(byid))
    print(mail_ru_data)
    login, pwd = mail_ru_data
    browser.find_element_by_name("login").send_keys(login)
    browser.find_element_by_name("password").send_keys(pwd)
    time.sleep(2)
    browser.find_element_by_id("mailbox:submit").click()


# жіЁе†ЊйЂџеЌ–йЂљ
def registeralie(vmid, browser, byid, surname, name, useragent, yzmpath, dbManagers, maileruaccount,
                 mailerupassword, starttime):
    AliexpressPassword = ""
    var = 1
    logid = str(int(time.time()))
    # browser.get("https://ru.aliexpress.com/")
    browser.get("https://login.aliexpress.com/join/buyer/expressJoin.htm")
    setlog(logid, "ж‰“ејЂдє†йЂџеЌ–йЂљзљ„дё»йЎµ", dbManagers, "A")
    setScriptStatus("ж‰“ејЂдє†йЂџеЌ–йЂљзљ„дё»йЎµ", dbManagers, vmid, "A", int(
        (datetime.datetime.now() - starttime).total_seconds()))
    time.sleep(5)
    while var < 100:
        try:
            var = var + 1
            time.sleep(1)
            if var > 50:
                print("еј‚еёёеѕЄзЋЇ")
                setlog(logid, "еЎ«иЎЁеј‚еёёеѕЄзЋЇпјЊй‡ЌеђЇи„љжњ¬", dbManagers, "C")
                return False
            setlog(logid, "иї›е…Ґз”іиЇ·йЎµ", dbManagers, "A")
            setScriptStatus("иї›е…Ґз”іиЇ·йЎµ", dbManagers, vmid, "A",
                            int((datetime.datetime.now() - starttime).total_seconds()))
            time.sleep(5)
            print("йЂџеЌ–йЂљжіЁе†ЊејЂе§‹")
            # # еЋ»й™¤еј№зЄ—
            # execute_script(browser, "document.querySelector('.close-layer').click();")
            # иѕ“е…Ґиґ¦еЏ·
            browser.execute_script(
                "document.querySelector('#ws-xman-register-email').value='';")
            browser.find_element_by_id(
                "ws-xman-register-email").send_keys(maileruaccount)
            setScriptStatus("иѕ“е…Ґиґ¦еЏ·" + maileruaccount, dbManagers, vmid, "A",
                            int((datetime.datetime.now() - starttime).total_seconds()))
            time.sleep(3)
            # иѕ“е…ҐеЇ†з Ѓ
            AliexpressPassword = getpasssord()
            browser.execute_script(
                "document.querySelector('#ws-xman-register-password').value='';")
            browser.find_element_by_id(
                "ws-xman-register-password").send_keys(AliexpressPassword)
            setScriptStatus("иѕ“е…ҐеЇ†з Ѓ" + AliexpressPassword, dbManagers, vmid, "A",
                            int((datetime.datetime.now() - starttime).total_seconds()))
            time.sleep(2)
            # зЎ®и®¤жіЁе†Њ
            execute_script(
                browser, "document.querySelector('#ws-xman-register-submit').click();")
            time.sleep(3)
            # зЎ®и®¤еЇ†з Ѓ
            # иЋ·еЏ–йЄЊиЇЃз Ѓй“ѕжЋҐ
        except:
            if len(execute_script(browser, "return document.querySelector('#form-searchbar').className;")) > 0 or len(
                    execute_script(browser, "return document.querySelector('.welcome').innerText;")) > 0:
                print("жіЁе†Њж€ђеЉџ")
                break
            else:
                continue
    #############################################з™»еЅ•й‚®з®±зЎ®и®¤###################################################
    var2 = 1
    browser.get("https://e.mail.ru/inbox")
    var3 = 1
    setlog(logid, "иї›е…Ґй‚®з®±ж”¶д»¶з®±з•Њйќў", dbManagers, "A")
    setScriptStatus("иї›е…Ґй‚®з®±ж”¶д»¶з®±з•Њйќў", dbManagers, vmid, "A", int(
        (datetime.datetime.now() - starttime).total_seconds()))
    while var3 < 100:
        if browser.current_url == "https://e.mail.ru/login":
            login_to_mail(dbManagers, byid, browser)
            continue
        var3 = var3 + 1
        try:
            time.sleep(1)
            if var3 > 50:
                print("еј‚еёёеѕЄзЋЇ")
                setlog(logid, "и„љжњ¬е› йЄЊиЇЃз ЃеѕЄзЋЇи§¦еЏ‘й‡ЌеђЇи„љжњ¬", dbManagers, "C")
                setScriptStatus("и„љжњ¬е› йЄЊиЇЃз ЃеѕЄзЋЇи§¦еЏ‘й‡ЌеђЇи„љжњ¬", dbManagers, vmid, "C",
                                int((datetime.datetime.now() - starttime).total_seconds()))
                return False
            if len(execute_script(browser, "return document.querySelector('.successA').innerText;")) > 0:
                setlog(logid, "жіЁе†Њж€ђеЉџ", dbManagers, "A")
                setScriptStatus("жіЁе†Њж€ђеЉџ", dbManagers, vmid, "A",
                                int((datetime.datetime.now() - starttime).total_seconds()))
                break
            # е…ій—­еј№зЄ—
            execute_script(browser, "document.querySelector('.c014').click();")
            execute_script(
                browser, "document.querySelector('.c01141').click();")
            execute_script(
                browser, "document.querySelector('.c0175').click();")
            execute_script(
                browser, "return document.querySelector('.js-next-step-button').click();")
            time.sleep(3)
            mailenum = execute_script(browser,
                                      "return document.querySelectorAll('.b-datalist__item__addr').length;")
            mailenum2 = execute_script(
                browser, "return document.querySelectorAll('.ll-crpt').length;")
            if mailenum > 0:
                for a in range(0, int(mailenum)):
                    if execute_script(browser,
                                      "return document.querySelectorAll('.b-datalist__item__addr')[{}].innerText;".format(str(a))) == "AliExpress.com":
                        execute_script(browser,
                                       "document.querySelectorAll('.b-datalist__item__addr')[{}].click();".format(str(a)))
                        time.sleep(5)
                        break
            elif mailenum2 > 0:
                for b in range(0, int(mailenum2)):
                    if execute_script(browser, "return document.querySelectorAll('.ll-crpt')[{}].innerText;".format(str(b))) == "AliExpress.com":
                        execute_script(browser,
                                       "document.querySelectorAll('.ll-crpt')[{}].click();".format(str(b)))
                        time.sleep(5)
                        break
            hrefnum = execute_script(
                browser, "return document.querySelectorAll('a').length;")
            if hrefnum != "":
                urlal = "accounts.aliexpress.com"
                for b in range(0, int(hrefnum)):
                    ailpeurl = execute_script(
                        browser, "return document.querySelectorAll('a')[{}].href;".format(str(b)))
                    if urlal in ailpeurl:
                        browser.get(ailpeurl)
                        time.sleep(5)
                        break
        except:
            continue
    if AliexpressPassword != "":
        return AliexpressPassword
    else:
        return False


# дїќе­дїЎжЃЇ
def savedata(dbManagers, year, month, day, account, password, AliexpressPassword, byid):
    dbManagers.execute(
        """UPDATE Buyer SET birthdayyear='{}',birthdaymonth='{}',EmailRegisteredTime='{}',
            birthdayday='{}', Email='{}', PassWord='{}',AliexpressRegisteredTime='{}',AliexpressAccount='{}',
            AliexpressPassword='{}' WHERE BuyerID='{}'""".format(
            year, month, str(datetime.datetime.now()), day, account, password, str(
                datetime.datetime.now()), account, AliexpressPassword, byid
        ))
    dbManagers.close()


# save registered mail.ru data
def save_mail_data(dbManagers, year, month, day, account, password, byid):
    dbManagers.execute("""UPDATE Buyer SET birthdayyear='{}',birthdaymonth='{}',EmailRegisteredTime='{}',
                            birthdayday='{}',Email='{}',PassWord='{}' WHERE BuyerID='{}'""".format(
        year, month, str(datetime.datetime.now()), day, account, password, byid
    ))
    dbManagers.close()


# save registered aliexpress data
def save_ali_data(dbManagers, account, aliexpress_password, byid):
    dbManagers.execute("UPDATE Buyer SET AliexpressAccount='{}',AliexpressPassword='{}',AliexpressRegisteredTime='{}' WHERE BuyerID='{}'".format(
        account, aliexpress_password, str(datetime.datetime.now()), byid
    ))
    dbManagers.close()


if __name__ == '__main__':
    # ејЂе§‹ж—¶й—ґ
    starttime = datetime.datetime.now()
    dbManager = DbManager()
    # ж №жЌ®и™љж‹џжњєidиЇ»еЏ–
    vmid = getVMConfig("VMInfo", "VMID")
    yzmpath = getConfig("vcode", "yzmpath")
    if not os.path.exists(yzmpath):
        os.makedirs(yzmpath)
    print("vmid" + vmid)
    # buyers range
    # еѕЄзЋЇе¤ље°‘ж¬Ўд»ЋиЎЁдё­иЇ»еЏ–
    controlres = dbManager.fetchone(
        "SELECT RunBuyerIDStart, RunBuyerIDEnd FROM Control where VMID ='{}'".format(vmid))
    end_buyer_id = int(controlres[1]) - 1
    print(controlres[1])
    print(vmid)
    # get info about last user in user range
    # жџҐиЇўжЇеђ¦е…ЁйѓЁжіЁе†Ње®Њ
    reend = dbManager.fetchone(
        """SELECT BuyerID, ENlastname, ENfirstname, BrowserUserAgent, ScreenResolutionX,
         ScreenResolutionY, Email FROM Buyer WHERE BuyerID ='{}' and VMID='{}'""".format(str(end_buyer_id), vmid))
    # email exists
    print(reend[6])
    if reend[6]:
        print("over")
    else:
        for i in range(int(controlres[0]), int(controlres[1])):
            print(i)
            time.sleep(5)
            # й‚®з®±з±»ећ‹0, 1, 2, 3 @ mail.ru @ inbox.ru @ list.ru @ bk.ru
            mailetype = random.randint(0, 3)
            re = dbManager.fetchone(
                """SELECT BuyerID, ENlastname, ENfirstname, ShaID, BrowserUserAgent, ScreenResolutionX , ScreenResolutionY,
                Email, AliexpressAccount FROM Buyer where BuyerID ='{}' and VMID='{}'""".format(
                    str(i), vmid
                ))
            # иґ¦еЏ·ID
            BuyerID = re[0]
            # е§“
            ENfirstname = re[1]
            # еђЌ
            ENlastname = re[2]
            # жµЏи§€е™Ёе±ћжЂ§
            BrowserInfo = re[4]
            # е±Џе№•е€†иѕЁзЋ‡X
            ScreenResolutionX = re[5]
            # е±Џе№•е€†иѕЁзЋ‡Y
            ScreenResolutionY = re[6]
            # и®ѕзЅ®е€†иѕЁзЋ‡
            # setscreen(int(ScreenResolutionX), int(ScreenResolutionY))
            # иґ¦еЏ·
            if re[8]:
                print("е·ІжіЁе†Њ")
                continue    
            logs = "жњ¬ж¬Ўз”іиЇ·й‚®з®±з±»ећ‹дёє: {}пјЊ е§“ пјљ{} еђЌ пјљ{} е±Џе№•е€†иѕЁзЋ‡X: {}  е±Џе№•е€†иѕЁзЋ‡Y  {}  жµЏи§€е™Ёе±ћжЂ§ {}".format(mailetype, str(ENfirstname),
                                                                                        str(ENlastname), str(ScreenResolutionX), str(ScreenResolutionY), str(BrowserInfo))
            print(str(logs))
            # ејЂе§‹жіЁе†Њ
            mare = re[7]
            while True:
                # и®ѕзЅ®зЅ‘з»њ
                if not setintnet(1):
                    print("зЅ‘з»њи®ѕзЅ®е¤±иґҐ")
                    input()
                    continue
                # е€ќе§‹еЊ–жµЏи§€е™Ё / initialize browser
                browser = setbrowser(BrowserInfo, BuyerID)
                if not browser:
                    browser.quit()
                    continue
                # жіЁе†Њmalru / register mail.ru
                if not mare:
                    resetintnet()
                    time.sleep(5)
                    mare = registermail(vmid, browser, BuyerID, ENfirstname, ENlastname, mailetype, BrowserInfo, yzmpath,
                                        dbManager,
                                        starttime)
                    # save mail.ru data
                    save_mail_data(
                        dbManager, mare[0], mare[1], mare[2], mare[3], mare[4], BuyerID)
                    if not mare:
                        browser.quit()
                        continue
                    # savedata(dbManager, mare[0], mare[1], mare[2], mare[3], mare[4], aplie, BuyerID)
                    # жіЁе†ЊйЂџеЌ–йЂљ / Register aliexpress
                    # mare[3] / mail.ru login
                    # mare[4] / mail.ru pwg
                    aplie = registeralie(vmid, browser, BuyerID, ENfirstname, ENlastname, BrowserInfo, yzmpath, dbManager,
                                         mare[3],
                                         mare[4], starttime)
                    save_ali_data(dbManager, mare[3], aplie, BuyerID)
                    if aplie == None:
                        browser.quit()
                        continue
                # if have mail and don't have aliexpress register only aliexpress
                elif mare and re[8] == None:
                    registered_email_for_aliexpress = dbManager.fetchone(
                        "SELECT Email, Password FROM Buyer WHERE BuyerID ='{}' and VMID='{}'".format(str(BuyerID), vmid))
                    aplie_reg = registeralie(vmid, browser, BuyerID, ENfirstname, ENlastname, BrowserInfo, yzmpath, dbManager,
                                         registered_email_for_aliexpress[0],
                                         registered_email_for_aliexpress[1], starttime)
                    # save aliexpress data
                    save_ali_data(
                        dbManager, registered_email_for_aliexpress[0], aplie_reg, BuyerID)
                # close browser and wait
                browser.quit()
                time.sleep(5)

                break
            time.sleep(3)
    input()
